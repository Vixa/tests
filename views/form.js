const FormView = Backbone.View.extend({
    tagName: 'form',
    template: '<input type="text"/>',
    events: {
        'submit': 'submit',
        'keyup input': 'processKey',
    },
    initialize: function () {
        this.list = new List({
            collection: this.collection
        });
        this.sortButtons = new SortView({parent:this.list});
        this.render();
    },
    render: function () {
        const app = document.getElementsByClassName('app')[0];
        app.innerHTML = '';
        this.el.innerHTML = this.template;
        app.appendChild(this.el);
        app.appendChild(this.sortButtons.el);
        app.appendChild(this.list.el);
    },
});

_.extend(FormView.prototype, FormMixin);