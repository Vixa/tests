const List = Backbone.View.extend({
    tagName: 'ul',
    className: 'states',
    initialize: function() {
        $.ajax(this.collection.url()).then(data => {
            Object.keys(data).forEach(key => {
                this.collection.add({
                    code: key,
                    title: data[key]
                });
            });

            this.states = this.collection;
            this.render();
        });
    },
    states: null,
    search: function(text) {
        this.states = this.collection.search(text);
        this.renderList();
    },
    sorts: function(type) {
        this.collection.sorts(this.states, type);
        this.renderList();
    },
    renderList: function() {
        this.$el.html('');
        this.states.each(state => {
            const li = new ListItem({
                model: state
            });
            this.$el.append(li.el);
        });
    },
    render: function () {
        this.renderList(this.collection);
        return this;
    }
});