const ListItem = Backbone.View.extend({
    tagName: 'li',
    template: _.template('<strong><%=title %></strong>'),
    initialize: function () {
        this.render();
    },
    render: function () {
        this.$el.html(this.template(this.model.toJSON()))
    }
});