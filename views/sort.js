const SortView = Backbone.View.extend({
    tagName: 'div',
    template: '<a data-type="desc">Sort by Title</a>',
    events: {
        'click a': 'sorts',
    },
    sorts: function(event) {
        event.preventDefault();
        const sortType = event.target.dataset.type;
        event.target.dataset.type = sortType === 'asc' ? 'desc' : 'asc';
        this.parent.sorts(sortType);
    },
    initialize: function (options) {
        this.parent = options.parent;
        this.render();
    },
    render: function () {
        this.$el.html(this.template);
    },
});