const StatesMixin = {
    search: function (text) {
        if(!text) return this;

        var pattern = new RegExp(text,"gi");
        const data = _(this.filter(function(data) {
            return pattern.test(data.get("title"));
        }));

        const clonedCollection = new Backbone.Collection();
        data.each(function(model) {
            clonedCollection.add(new Backbone.Model(model.toJSON()));
        });

        return clonedCollection;
    },
    sorts: function (collection, direction) {
        const sortBy = 'title';
        collection.comparator = function(item1, item2) {
            if(direction === 'desc') {
                if (item1.get(sortBy) < item2.get(sortBy))
                    return 1;
                if ( item1.get(sortBy) > item2.get(sortBy))
                    return -1;
            } else {
                if (item1.get(sortBy) < item2.get(sortBy))
                    return -1;
                if ( item1.get(sortBy) > item2.get(sortBy))
                    return 1;
            }

            return 0;
        };
        collection.sort();
    }
};