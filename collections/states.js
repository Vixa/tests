const StatesCollection = Backbone.Collection.extend({
    model: StatesModel,
    comparator: 'title',
    url: function() {
        return './states_hash.json';
    }
});
_.extend(StatesCollection.prototype, StatesMixin);